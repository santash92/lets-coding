﻿namespace WebApplicationSelect.Models;

public class DemoModel
{
    public string? SelectedText { get; set; }
    public string? HiddenValue { get; set; }
}
